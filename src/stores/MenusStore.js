import { observable, action, decorate } from 'mobx';

class MenusStore {
  service = null;

  menus = [];

  selectedMenuIndex = 0;

  constructor(service) {
    this.service= service;
  }

  load = (contextType) => {
    if (this.service) {
      // read categories from local storage
      console.log("%c Reading menus...", 'color: green');

      this.menus = this.service.load(contextType);
    }
  }

  setSelectedMenuIndex = (value) => {
    this.selectedMenuIndex = value;
  }
}

const menusStore = decorate(MenusStore, {
  menus:            observable,
  load:             action,


  selectedMenuIndex: observable,
  setSelectedMenuIndex: action,
});

export default menusStore;
