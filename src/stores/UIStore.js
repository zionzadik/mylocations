import { observable, action, decorate } from 'mobx';

class UIStore {
  notification;
  viewType = "list";
  sortAsc = true;

  setNotification = (variant, message) => {
    this.notification = {
      variant: variant,
      message: message
    }
  }

  setViewType = (type) => {
    this.viewType = type;
  }

  toggleSortOrder = () => {
    this.sortAsc = !this.sortAsc;
  }

}

const uiStore = decorate(UIStore, {
  notification:     observable,
  setNotification:  action,

  viewType:         observable,
  setViewType:      action,

  sortAsc:          observable,
  toggleSortOrder:  action,
});

export default uiStore;
