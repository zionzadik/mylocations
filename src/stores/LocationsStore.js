import { observable, action, decorate } from 'mobx';
import Location from '../model/Location';

class LocationsStore {
  service = null;

  locations = [];

  selectedLocation;

  constructor(service) {
    this.service= service;
  }

  sort = (asc) => {
    if (this.service) {
      const locations = this.service.sort(this.locations, asc);
      this.set(locations);
    }
  }

  load = () => {
    if (this.service) {
      // read categories from local storage
      console.log("%c Reading all locations...", 'color: green');

      const locations = this.service.load();
      this.set(locations);
    } else {
      throw new Error("Service not set");
    }
  }

  add = (name, caterories, coordinates, address) => {
    if (this.service) {
      try {
        const locations = this.service.add(new Location(name, caterories, coordinates, address));
        this.set(locations);
      }
      catch(e) {
        console.log('%c Location already exists', 'color: red');
        throw e;
      }
    } else {
      throw new Error("Service not set");
    }
  }

  edit = (location, name, caterories, coordinates, address) => {
    if (this.service) {
      const newLocation = new Location(name, caterories, coordinates, address);
      const locations = this.service.replace(location, newLocation);
      this.set(locations);
      this.setSelectedLocation(newLocation);
    } else {
      throw new Error("Service not set");
    }
  }

  delete = (location) => {
    if (this.service) {
      const locations = this.service.delete(location);
      this.set(locations);
      this.setSelectedLocation(undefined);
    } else {
      throw new Error("Service not set!");
    }
  }

  deleteByCategory = (category) => {
    if (this.service) {
      const locations = this.service.deleteByCategory(this.locations, category);
      this.set(locations);
      this.setSelectedLocation(undefined);
    } else {
      throw new Error("Service no set!");
    }
  }

  groupByCategory = (list, keyGetter, categoryNameGetter, sortAsc) => {
    if (this.service) {
      return this.service.groupByCategory(list, keyGetter,categoryNameGetter, sortAsc);
    }
  }

  set = (locations) => {
    this.locations = locations;
  }

  setSelectedLocation = (location) => {
    this.selectedLocation = location;
  }
}

const locationsStore = decorate(LocationsStore, {
  locations:            observable,
  set:                  action,
  load:                 action,
  sort:                 action,
  add:                  action,
  edit:                 action,
  delete:               action,

  selectedLocation:     observable,
  setSelectedLocation:  action,
});

export default locationsStore;
