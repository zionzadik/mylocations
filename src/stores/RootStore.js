import { RouterState, RouterStore } from 'mobx-state-router';
import services from '../services';
import CategoriesStore from './CategoriesStore';
import LocationsStore from './LocationsStore';
import MenusStore from './MenusStore';
import UIStore from './UIStore';

import routes from '../routes';

const notFound = new RouterState('notFound');

export class RootStore {
    categoriesStore   = new CategoriesStore(services.categoryService);
    locationsStore    = new LocationsStore(services.locationService);
    menusStore        = new MenusStore(services.menuService);
    UIStore           = new UIStore();
    routerStore       = new RouterStore(this, routes, notFound);

    // ApiServices for use by all stores
    services = services;
}
