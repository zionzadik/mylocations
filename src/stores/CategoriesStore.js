import { observable, action, decorate } from 'mobx';
import Category from '../model/Category';

class CategoriesStore {
  service = null;

  categories = [];

  selectedCategory;

  constructor(service) {
    this.service= service;
  }

  load = () => {
    if (this.service) {
      // read categories from local storage
      console.log("%c Reading all categories...", 'color: green');

      const categories = this.service.load();
      this.set(categories);
    } else {
      throw new Error("Service not set");
    }
  }

  add = (name) => {
    if (this.service) {
      try {
        const category = new Category(name);
        const categories = this.service.add(category);
        this.set(categories);
        return category;
      }
      catch(e) {
        console.log(e)
        console.log('%c Category already exists', 'color: red');
        throw e;
      }
    } else {
      throw new Error("Service not set");
    }
  }

  edit = (categoryId, name) => {
    if (this.service) {
      const categories = this.service.replace(categoryId, name);
      const idx = this.service.indexOf(categories, "id", categoryId)
      this.set(categories);
      this.setSelectedCategory(categories[idx]);
    } else {
      throw new Error("Service not set");
    }
  }

  delete = (category, locationDeleteFunc) => {
    if (this.service) {
      locationDeleteFunc(category);
      const categories = this.service.delete(category);
      this.set(categories);
      this.setSelectedCategory(undefined);
    } else {
      throw new Error("Service not set!");
    }
  }

  set = (categories) => {
    this.categories = categories;
  }

  setSelectedCategory = (category) => {
    this.selectedCategory = category;
  }
}

const categoriesStore = decorate(CategoriesStore, {
  categories:           observable,
  set:                  action,
  load:                 action,
  add:                  action,
  edit:                 action,
  delete:               action,

  selectedCategory:     observable,
  setSelectedCategory:  action,
});

export default categoriesStore;
