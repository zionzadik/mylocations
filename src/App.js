import React from 'react';
import './App.css';

import { Provider } from 'mobx-react';
import { HistoryAdapter } from 'mobx-state-router';
import { RootStore} from './stores/RootStore';
import { createBrowserHistory } from 'history';

import { ThemeProvider as MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import { theme, styles } from './components/ThemeStyle';

import Header from './components/Header';
import Footer from './components/Footer';

import { Shell } from './shell';

// Create the rootStore
const rootStore = new RootStore();

// Observe history changes
const history = createBrowserHistory();
const historyAdapter = new HistoryAdapter(rootStore.routerStore, history);
historyAdapter.observeRouterStateChanges();

const App = (props) => {
  const { classes } = props;

  return (
    <MuiThemeProvider theme={theme}>
      <Provider rootStore={rootStore}>
      <div className={classes.root}>
        <CssBaseline />
        <div className={classes.appContent}>
          <Header />
          <main className={classes.mainContent}>
            <Shell />
          </main>
          <Footer />
        </div>
      </div>
      </Provider>
    </MuiThemeProvider>
  )
}


App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
