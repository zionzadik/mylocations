import uuid from 'uuid/v1';

export default class BaseEntity {
  name;
  id;

  constructor(name) {
    this.id = uuid();
    this.name = name;
  }

}
