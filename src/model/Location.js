import BaseEntity from './BaseEntity';

export default class Location extends BaseEntity {
  categories;
  coordinates;
  address;

  constructor(name, categories, coordinates, address) {
    super(name);

    this.categories = categories;
    this.coordinates = coordinates; // probably need to verify the geocode
    this.address = address;
  }
}
