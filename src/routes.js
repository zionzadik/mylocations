const routes = [
  {
    name: 'categories',
    pattern: '/categories',
    onEnter: (fromState, toState, routerStore) => {
      const {
          rootStore: { categoriesStore }
      } = routerStore;

      //load categories data
      categoriesStore.load();
      return Promise.resolve();
    }
  },
  {
    name: 'home',
    pattern: '/',
    onEnter: (fromState, toState, routerStore) => {
      const {
          rootStore: { categoriesStore }
      } = routerStore;

      //load categories data
      categoriesStore.load();
      return Promise.resolve();
    }
  },
  {
    name: 'locations',
    pattern: '/locations',
    onEnter: (fromState, toState, routerStore)=> {
      const {
          rootStore: { locationsStore, categoriesStore }
      } = routerStore;

      //load locations data
      categoriesStore.load();
      locationsStore.load();
      return Promise.resolve();
    }
  },
  {
      name: 'notFound',
      pattern: '/not-found'
  }
];

export default routes;
