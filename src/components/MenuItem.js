import React from 'react';

import Tab from '@material-ui/core/Tab';

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const MenuItem = (props) => {
  const { label, icon, disabled, index } = props;

  return (
    <Tab label={label} icon={icon} disabled={disabled} {...a11yProps(index)}/>
  );
}

//export default MenuItem;
