import React, { useEffect } from 'react';

import { makeStyles } from '@material-ui/core/styles';

import { inject, observer } from 'mobx-react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
import Input from '@material-ui/core/Input';

const useStyles = makeStyles(theme => ({
  select: {
    marginTop: theme.spacing(2),
    minWidth: 225,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
    backgroundColor: theme.palette.primary.main,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const CategoriesSelect= (props) => {
  const { rootStore: { categoriesStore }} = props;
  const classes = useStyles();
  const [categoryItems, setCategoryItems] = React.useState(null);

  const handleChange = (event) => {
    props.onChange(event.target.value);
  };

  useEffect(() => {
    if (categoriesStore.categories) {
      const categoriesArr = categoriesStore.categories.map((category, index) => {
        return <MenuItem value={category.id} key={index}>{category.name} </MenuItem>
      });

      setCategoryItems(categoriesArr);
    }
    else {

    }

  }, [categoriesStore.categories]);

  return (
    <Select className={classes.select}
      labelId="demo-mutiple-chip-label"
      id="demo-mutiple-chip"
      multiple
      disabled={props.readonly}
      displayEmpty
      value={props.value}
      onChange={handleChange}
      input={<Input id="select-multiple-chip" />}
      renderValue={selected => {
        if (selected.length === 0) {
          return <em>Select Category</em>;
        }

        return (
          <div className={classes.chips}>
            {selected.map(value => {
              const category = categoriesStore.categories.find(element => element.id === value);
              return (
                <Chip key={value} label={category.name} className={classes.chip} />
              )
            })}
          </div>
      )}}
      MenuProps={MenuProps}
    >
      {categoryItems}
    </Select>
  )
}

export default inject('rootStore')(observer(CategoriesSelect));
