import React from 'react';
import { inject, observer } from 'mobx-react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const CategoryItem = ({category, rootStore}) => {
  const categoriesStore = rootStore.categoriesStore;

  const handleListItemClick = (event, category) => {
    categoriesStore.setSelectedCategory(category);
  };

  return (
    <ListItem
      button
      selected={categoriesStore.selectedCategory === category}
      onClick={event => handleListItemClick(event, category)}
    >
      <ListItemText primary={category.name} />
    </ListItem>
  )
}

export default inject('rootStore')(observer(CategoryItem));
