import React from 'react';
import { inject, observer } from 'mobx-react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    margin: '10px'
  }
}));


const CategoriesDeleteConfirmation = (props) => {
  const { rootStore: { categoriesStore, locationsStore, UIStore, menusStore }} = props;
  const classes = useStyles();

  const handleSubmit = (event) => {
    try {
      if (props.context) {
        categoriesStore.delete(props.context, locationsStore.deleteByCategory);
        menusStore.setSelectedMenuIndex(0);
        UIStore.setNotification("success", "Category deleted succesfuly!");
      }
    } catch(e) {
      UIStore.setNotification("error", e.message);
    }
  }
  return (
    <div>
      <div>
        <Typography variant="h6">
          Are you sure you want to delete the selected category?
        </Typography>
        <Typography variant="h6">
          This will delete the category and any locations using it.
        </Typography>
      </div>
      <div>
        <Button className={classes.root} variant="contained" color="secondary" onClick={handleSubmit}>Delete</Button>
      </div>
    </div>
  )
}

export default inject('rootStore')(observer(CategoriesDeleteConfirmation));
