import React from 'react';
import { inject, observer } from 'mobx-react';

import CategoryForm from './CategoryForm';
import CategoriesDeleteConfirmation from './CategoriesDeleteConfirmation';
import CategoryDetails from './CategoryDetails';

import TabPanel from '../TabPanel';

const CategoryPanels = (props) => {
  const categoriesStore = props.rootStore.categoriesStore;

  React.useEffect(() => {
    //console.log("CategoryPanels", categoriesStore.selectedCategory);
  }, [categoriesStore.selectedCategory]);

  return (
    <React.Fragment>
      <TabPanel value={props.value} index={0}>
        <CategoryForm context={null} />
      </TabPanel>
      <TabPanel value={props.value} index={1}>
        <CategoryForm context={categoriesStore.selectedCategory} />
      </TabPanel>
      <TabPanel value={props.value} index={2}>
        <CategoriesDeleteConfirmation context={categoriesStore.selectedCategory}/>
      </TabPanel>
      <TabPanel value={props.value} index={3}>
        <CategoryDetails context={categoriesStore.selectedCategory}/>
      </TabPanel>
    </React.Fragment>
  );
}

export default inject('rootStore')(observer(CategoryPanels));
