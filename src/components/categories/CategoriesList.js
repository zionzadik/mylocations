import React from 'react';
import { inject, observer } from 'mobx-react';

import List from '@material-ui/core/List';

import CategoryItem from './CategoryItem';

const CategoriesList = (props) => {
  const { rootStore: { categoriesStore }} = props;
  const [categoryItems, setCategoryItems] = React.useState(null);

  React.useEffect(() => {
    if (categoriesStore.categories) {
      const categoriesArr = categoriesStore.categories.map((category, index) => {
        return <CategoryItem category={category} key={index} />
      });

      setCategoryItems(categoriesArr);
    }
  }, [categoriesStore.categories]);

  return (
    <div>
      <List>
        {categoryItems}
      </List>
    </div>
  )
}

export default inject('rootStore')(observer(CategoriesList));
