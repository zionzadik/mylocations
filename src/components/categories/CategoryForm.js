import React from 'react';
import { inject, observer } from 'mobx-react';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    margin: '10px'
  }
}));

const CategoryForm = (props) => {
  const { rootStore: { categoriesStore, UIStore }} = props;
  const classes = useStyles();

  const [ name, setName ] = React.useState(props.context ? props.context.name : "");

  React.useEffect(() => {
    setName((props.context && props.context.name) ? props.context.name : "");
  }, [props.context]);

  const handleNameChange = (event, value) => {
    setName(event.target.value);
  }

  const resetForm = () => {
    setName("");
  }

  const handleSubmit = (event) => {
    try {
      if (props.context) {
        categoriesStore.edit(props.context.id, name);
        UIStore.setNotification("success", "Category updated succesfuly!");
      } else {
        const newCategory = categoriesStore.add(name);
        UIStore.setNotification("success", "Category Added succesfuly!");
        resetForm();

        if (props.onSubmit) {
          props.onSubmit(newCategory);
        }
      }


    } catch(e) {
      UIStore.setNotification("error", e.message);
    }
  }

  return (
    <div>
      <form noValidate autoComplete="off">
        <div>
          <TextField
            label="Name"
            required
            size="small"
            id="outlined-size-normal"
            variant="outlined"
            onChange={handleNameChange}
            value={name}
          />
        </div>
        <div>
          <Button className={classes.root} variant="contained" color="primary" onClick={handleSubmit} disabled={!name}>Save</Button>
        </div>
      </form>
    </div>
  )
}

export default inject('rootStore')(observer(CategoryForm));
