import React from 'react';
import { inject } from 'mobx-react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import ListIcon from '@material-ui/icons/List';
import LocationOnIcon from '@material-ui/icons/LocationOn';

const useStyles = makeStyles(theme => ({
  root: {
    width: 500,
  },
}));

const Footer = (props) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const { rootStore: { routerStore }} = props;

  return (
    <div>
      <BottomNavigation
        value={value}
        onChange={(event, newValue) => {
          routerStore.goTo(newValue);
          setValue(newValue);
        }}
        showLabels
        className={classes.root}
      >
        <BottomNavigationAction label="Categories" value="home" icon={<ListIcon />} />
        <BottomNavigationAction label="Locations" value="locations" icon={<LocationOnIcon />} />
      </BottomNavigation>
    </div>
  );
}

export default inject('rootStore')(Footer);
