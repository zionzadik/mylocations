import React from 'react';
import { inject, observer } from 'mobx-react';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContentWrapper from './SnackbarContentWrapper';


const Notification = (props) => {
  const { rootStore: { UIStore }} = props;

  const [open, setOpen] = React.useState(false);

  React.useEffect(() => {
    setOpen(true);
  }, [UIStore.notification]);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  return (
    <div>
      {UIStore.notification &&
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={open}
          autoHideDuration={2000}
          onClose={handleClose}
        >
          <SnackbarContentWrapper
            onClose={handleClose}
            variant={UIStore.notification.variant}//"success"
            message={UIStore.notification.message}//"This is a success message!"
          />
        </Snackbar>
      }
    </div>
  );
}

export default inject('rootStore')(observer(Notification));
