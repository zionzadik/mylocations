import React from 'react';
import { inject, observer } from 'mobx-react';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

function menuProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const Menu = (props) => {
  const { rootStore: { menusStore }} = props;

  const [ menuItems, setMenuItems ] = React.useState(null);
  const [ value, setValue ] = React.useState(menusStore.selectedMenuIndex);
  const [ contextType, setContextType] = React.useState(props.contextType);

  React.useEffect(() => {
    menusStore.load(props.contextType);
    if (contextType !== props.contextType) {
      // we only want to change the selected panel is the type of object has changed
      menusStore.setSelectedMenuIndex(0);
      setContextType(props.contextType);
    }
  }, [props.context]);

  const handleChange = (event, newValue) => {
    props.onChange(newValue);
  };

  const handleTabClick = (index) => (event)=> {
    menusStore.setSelectedMenuIndex(index);
  }
  React.useEffect(() => {
    if (menusStore.menus && menusStore.menus.length > 0) {
      const menuItems = menusStore.menus.map((menu, index) => {
        return <Tab
                  label={menu.label}
                  onClick={handleTabClick(index)}
                  icon={menu.icon}
                  disabled={!menu.isActive(props.context)}
                  key={index}
                />
      });

      setMenuItems(menuItems);
    }
  }, [menusStore.menus]);

  return (
    <div>
      <Tabs value={menusStore.selectedMenuIndex} onChange={handleChange}>
        {menuItems}
      </Tabs>
    </div>
  )
}

export default inject('rootStore')(observer(Menu));
