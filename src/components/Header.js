import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const Header = (props) => {
  return (
    <React.Fragment>
      <AppBar color="primary" position="sticky" elevation={5}>
        <Toolbar>
          <Typography variant="h6">
            My Locations
          </Typography>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}

export default Header;
