import React, { useEffect, useState } from 'react';
import { inject, observer } from 'mobx-react';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';

import CategoriesSelect from '../categories/CategoriesSelect';
import CategoryForm from '../categories/CategoryForm';
import NewLocationMap from './NewLocationMap';

const useStyles = makeStyles(theme => ({
  root: {
    margin: '10px'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
    display: "block"
  },
}));

const LocationForm = (props) => {
  const { rootStore: { locationsStore, categoriesStore, UIStore }} = props;
  const classes = useStyles();
  const [ location, setLocation ] = useState(props.context);

  useEffect(() => {
    setLocation(props.context);
  }, [props.context]);

  const handleNameChange = event => {
    setLocation({...location, name: event.target.value});
  }

  const handleCategoryChange = (value) => {
    debugger;
    setLocation({...location, categories: value});
  }

  const handleCoordinatesChange = (value) => {
    const coords = { lat: value.coordinates.lat, lng: value.coordinates.lng };
    setLocation({...location, coordinates: coords, address: value.address});
  }

  const handleAddressChange = (value) => {
    setLocation({...location, address: value});
  }

  const resetForm = () => {
    setLocation({
      name: "",
      category: [],
      coordinates: null,
      address: null
    })
  }

  const handleSubmit = (event) => {
    try {
      if (props.context) {
        locationsStore.edit(props.context, location.name, location.categories, location.coordinates, location.address);
        UIStore.setNotification("success", "Location updated succesfuly!");
      } else {
        locationsStore.add(location.name, location.categories, location.coordinates, location.address);
        UIStore.setNotification("success", "Location Added succesfuly!");
        resetForm();
      }
    } catch(e) {
      UIStore.setNotification("error", e.message);
    }
  }

  const handleCategoryFormSubmit = (value) => {
    setLocation({...location, category: [value]});
  }

  const API_KEY = "AIzaSyDQsnA_-J6ei2bh5aK8znf29ym0i4rNP5A";

  return (
    <div>
      { categoriesStore.categories && categoriesStore.categories.length > 0 &&
        <form noValidate autoComplete="off">
          <FormControl className={classes.formControl}>
            <TextField
              label="Name"
              required
              size="small"
              id="outlined-size-normal"
              variant="outlined"
              onChange={handleNameChange}
              value={location ? location.name : ""}
            />
          </FormControl>
          <FormControl className={classes.formControl}>
            <CategoriesSelect value={(location && location.categories) ? location.categories: []} onChange={handleCategoryChange} />
          </FormControl>
          <NewLocationMap location={location ? location.coordinates: null} onCoordinatesChange={handleCoordinatesChange} />
          <div>
            <Button
              className={classes.root}
              variant="contained"
              color="primary"
              onClick={handleSubmit}
              disabled={!location || !location.name || (location.category && location.category.length) === 0}
            >
              Save
            </Button>
          </div>
        </form>
      }

      { categoriesStore.categories && categoriesStore.categories.length === 0 &&
        <div>
          <div>No categories created yet. Please create a category to continue creating the location</div>
          <CategoryForm context={null} onSubmit={handleCategoryFormSubmit}/>
        </div>
      }
    </div>
  )
}

export default inject('rootStore')(observer(LocationForm));
