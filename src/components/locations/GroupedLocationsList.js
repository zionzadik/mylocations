import React from 'react';
import { inject, observer } from 'mobx-react';

import { makeStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import StyledTreeItem from '../StyledTreeItem';
import ListIcon from '@material-ui/icons/List';
import LocationOnIcon from '@material-ui/icons/LocationOn';

const useStyles = makeStyles({
  root: {
    height: 264,
    flexGrow: 1,
    maxWidth: 400,
  },
});

const GroupedLocationsList = (props) => {
  const classes = useStyles();
  const { rootStore: { locationsStore, categoriesStore, UIStore, menusStore }} = props;

  const [locationItems, setLocationItems] = React.useState(null);
  const [ groupedLocations, setGroupedLocations] = React.useState(null);


  React.useEffect(() => {
    if (locationsStore.locations) {
      const locationsArr = locationsStore.groupByCategory(locationsStore.locations, location => location.categories, categoriesStore.service.get, UIStore.sortAsc );

      setGroupedLocations(locationsArr);
    }
  }, [locationsStore.locations, UIStore.sortAsc]);

  React.useEffect(() => {
    if (groupedLocations) {
      const result = [];
      let i = 0;
      for (const entry of groupedLocations.entries()) {
        const catLocations = entry[1];
        i++;
        let item = <StyledTreeItem context={entry[0]} onClickItem={onCategoryItemClicked} nodeId={i.toString()} key={i} labelText={entry[0]} labelIcon={ListIcon} >
          {
            catLocations.map((location, n) => {
              return (<StyledTreeItem
                context={location}
                onClickItem={onLocationItemClicked}
                nodeId={i.toString() + '_' + n.toString()}
                key={n}
                labelText={location.name}
                labelIcon={LocationOnIcon}
                color="#1a73e8"
                bgColor="#e8f0fe"
              />)
            })}
          </StyledTreeItem>;

        result.push(item);
      }

      setLocationItems(result);
    }
  }, [groupedLocations]);

  const onLocationItemClicked = (item) => {
    locationsStore.setSelectedLocation(item);
    menusStore.setSelectedMenuIndex(1); // view the "edit" panel. probably needs to be done differently
  }

  const onCategoryItemClicked = (item) => {
    //do nothing
  }

  return (
    <TreeView
      className={classes.root}
      defaultExpanded={['3']}
      defaultCollapseIcon={<ArrowDropDownIcon />}
      defaultExpandIcon={<ArrowRightIcon />}
      defaultEndIcon={<div style={{ width: 24 }} />}
    >
      {locationItems}
    </TreeView>
  );
}

export default inject('rootStore')(observer(GroupedLocationsList));
