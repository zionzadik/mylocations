import React from 'react';
import { inject, observer } from 'mobx-react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    margin: '10px'
  }
}));


const LocationDeleteConfirmation = (props) => {
  const { rootStore: { locationsStore, UIStore, menusStore }} = props;
  const classes = useStyles();

  const handleSubmit = (event) => {
    try {
      if (props.context) {
        locationsStore.delete(props.context);
        menusStore.setSelectedMenuIndex(0);
        UIStore.setNotification("success", "Location deleted succesfuly!");
      }
    } catch(e) {
      UIStore.setNotification("error", e.message);
    }
  }
  return (
    <div>
      <div>
        <Typography variant="h6">
          Are you sure you want to delete the selected location?
        </Typography>
      </div>
      <div>
        <Button className={classes.root} variant="contained" color="secondary" onClick={handleSubmit}>Delete</Button>
      </div>
    </div>
  )
}

export default inject('rootStore')(observer(LocationDeleteConfirmation));
