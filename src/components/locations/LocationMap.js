import React from "react";
import { observer } from 'mobx-react';
s
const LocationMap = (props) => {
  const [address, setAddress] = React.useState(props.address);
  const [coordinates, setCoordinates] = React.useState(props.coordinates);

  React.useEffect(() => {
    if (!navigator.geolocation) {
      console.log('Geolocation is not supported by your browser');
    } else {
      navigator.geolocation.getCurrentPosition(succes, error);
    }
  }, []);

  const onCoordinatesChange = (value) => {
    if (props.onCoordinatesChange) {
      props.onCoordinatesChange(value);
    }

    setCoordinates(value.coordinates);
    setAddress(value.address);
  }

  const success = (position) => {
    const latitude  = position.coords.latitude;
    const longitude = position.coords.longitude;

    setCoordinates({ lat: latitude, lng: longitude});
  }

  function error() {
    console.log('Unable to retrieve your location');
  }

  return (
    <MapContainer coordinates={coordinates} onCoordinatesChange={onCoordinatesChange}/>
  );
};
export default observer(LocationMap);
