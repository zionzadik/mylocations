import React from 'react';
import { inject, observer } from 'mobx-react';

import List from '@material-ui/core/List';

import LocationItem from './LocationItem';

const SimpleLocationsList = (props) => {
  const { rootStore: { locationsStore, UIStore, menusStore }} = props;
  const [locationItems, setLocationItems] = React.useState(null);

  const handleLocationClick = (item) => {
    menusStore.setSelectedMenuIndex(1); // view the "edit" panel. probably needs to be done differently
  }

  React.useEffect(() => {
    locationsStore.sort(UIStore.sortAsc);
  }, [UIStore.sortAsc]);

  React.useEffect(() => {
    if (locationsStore.locations) {
      const locationsArr = locationsStore.locations.map((location, index) => {
        return <LocationItem location={location} key={index} onClick={handleLocationClick}/>
      });

      setLocationItems(locationsArr);
    }
  }, [locationsStore.locations]);

  return (
    <div>
      <List>
        {locationItems}
      </List>
    </div>
  )
}

export default inject('rootStore')(observer(SimpleLocationsList));
