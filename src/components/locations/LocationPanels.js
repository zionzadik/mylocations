import React from 'react';
import { inject, observer } from 'mobx-react';

import LocationForm from './LocationForm';
import LocationDeleteConfirmation from './LocationDeleteConfirmation';
import LocationDetails from './LocationDetails';

import TabPanel from '../TabPanel';

const LocationPanels = (props) => {
  const {rootStore: { locationsStore, menusStore }} = props;

  return (
    <React.Fragment>
      <TabPanel value={menusStore.selectedMenuIndex} index={0}>
        <LocationForm context={null} />
      </TabPanel>
      <TabPanel value={menusStore.selectedMenuIndex} index={1}>
        <LocationForm context={locationsStore.selectedLocation} />
      </TabPanel>
      <TabPanel value={menusStore.selectedMenuIndex} index={2}>
        <LocationDeleteConfirmation context={locationsStore.selectedLocation} />
      </TabPanel>
      <TabPanel value={menusStore.selectedMenuIndex} index={3}>
        <LocationDetails context={locationsStore.selectedLocation} />
      </TabPanel>
    </React.Fragment>
  );
}

export default inject('rootStore')(observer(LocationPanels));
