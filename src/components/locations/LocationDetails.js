import React from 'react';
import { inject, observer } from 'mobx-react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import NewLocationMap from './NewLocationMap';
import CategoriesSelect from '../categories/CategoriesSelect';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});
const LocationDetails = (props) => {
  const classes = useStyles();

  console.log('--------', props.context);
  debugger;
  return (
    <div>
      <div>
        <Typography variant="h6">
          Location Details
        </Typography>
      </div>
      <div>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Property</TableCell>
                <TableCell>Value</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell component="th" scope="row">
                  Id
                </TableCell>
                <TableCell component="th" scope="row">
                  {props.context.id}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Name
                </TableCell>
                <TableCell component="th" scope="row">
                  {props.context.name}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Categories
                </TableCell>
                <TableCell component="th" scope="row">
                  <CategoriesSelect value={(props.context && props.context.categories) ? props.context.categories: []} readonly={true}/>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Coordinates
                </TableCell>
                <TableCell component="th" scope="row">
                  <NewLocationMap location={props.context.coordinates ? props.context.coordinates: null} readonly={true} />
                </TableCell>
              </TableRow>

            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  )
}

export default inject('rootStore')(observer(LocationDetails));
