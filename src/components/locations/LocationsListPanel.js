import React from 'react';
import { inject, observer } from 'mobx-react';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import ListIcon from '@material-ui/icons/List';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

const LocationListPanel = (props) => {
  const { rootStore: { UIStore }} = props;
  const classes = useStyles();

  const setViewType = (type) => {
    UIStore.setViewType(type);
  }

  const toggleSortOrder = () => {
    UIStore.toggleSortOrder();
  }

  return (
    <div className={classes.root}>
      <Tooltip title="View Grouped" aria-label="view grouped">
        <Fab size="small" color="primary" onClick={() => setViewType("grouped")}>
          <AccountTreeIcon />
        </Fab>
      </Tooltip>
      <Tooltip title="View List" aria-label="view list">
        <Fab size="small" color="primary" onClick={() => setViewType("list")}>>
          <ListIcon />
        </Fab>
      </Tooltip>
      <Tooltip title="Sort" aria-label="sort">
        <Fab size="small" color="primary" variant="extended" onClick={() => toggleSortOrder()}>
          {UIStore.sortAsc &&
            <ArrowDownwardIcon />
          }
          {!UIStore.sortAsc &&
            <ArrowUpwardIcon />
          }
          Sort
        </Fab>
      </Tooltip>
    </div>
  );
}

export default inject('rootStore')(observer(LocationListPanel));
