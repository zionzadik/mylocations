import React from 'react';
import StyledTreeItem from '../StyledTreeItem';

const LocationTreeItem= ({ location }) => {
  return (
    <StyledTreeItem nodeId={location.id} labelText={location.name} labelIcon={MailIcon} />
  )
}

export default LocationTreeItem;
