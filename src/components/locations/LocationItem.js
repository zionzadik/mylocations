import React from 'react';
import { inject, observer } from 'mobx-react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const LocationItem = ({location, rootStore, onClick}) => {
  const {locationsStore} = rootStore;

  const handleListItemClick = (event, location) => {
    locationsStore.setSelectedLocation(location);
    if (onClick)
      onClick(location);
  };

  return (
    <ListItem
      button
      selected={locationsStore.selectedLocation === location}
      onClick={event => handleListItemClick(event, location)}
    >
      <ListItemText primary={location.name} />
    </ListItem>
  )
}

export default inject('rootStore')(observer(LocationItem));
