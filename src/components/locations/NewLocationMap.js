import React, { useEffect, useState, useRef } from 'react';
import { inject, observer } from 'mobx-react';
import GoogleMapsApiLoader from "google-maps-api-loader";

const API_KEY = "AIzaSyDQsnA_-J6ei2bh5aK8znf29ym0i4rNP5A";

const initialConfig = {
  zoom: 12,
  center: { lat: 32.109333, lng: 34.855499 }
};

const NewLocationMap = (props) => {
  const { rootStore: { locationsStore }} = props;

  const [googleMap, setGoogleMap] = useState(null);
  const [map, setMap] = useState(null);
  const [geocoder, setGeocoder] = useState(null);
  const [autocomplete, setAutocomplete] = useState(null);
  const [isMarkerShown, setIsMarkerShown] = useState(false);
  const [marker, setMarker] = useState(null);
  // const [address, setAddress] = useState(null);
  // const [location, setLocation] = useState(props.location);

  const mapContainerRef = useRef(null);
  const searchTextField = useRef(null);

  useEffect(() => {
    GoogleMapsApiLoader({ libraries: ['places'], apiKey: "AIzaSyDQsnA_-J6ei2bh5aK8znf29ym0i4rNP5A" }).then(google => {
      setGoogleMap(google);
      setGeocoder()
    });
  }, []);

  useEffect(() => {
    if (searchTextField && googleMap) {
      setAutocomplete(new googleMap.maps.places.Autocomplete(searchTextField.current));
    }
  }, [searchTextField, googleMap]);

  useEffect(() => {
    if (autocomplete && map && searchTextField && geocoder) {
      autocomplete.setFields(['place_id', 'geometry', 'name', 'formatted_address']);
      map.controls[googleMap.maps.ControlPosition.TOP_LEFT].push(searchTextField.current);

      autocomplete.addListener('place_changed', () => {
        const place = autocomplete.getPlace();

        if (!place.place_id) {
          return;
        }
        geocoder.geocode({'placeId': place.place_id}, function(results, status) {
          if (status !== 'OK') {
            console.log('Geocoder failed due to: ' + status);
            return;
          }
          handleLocationChange(results[0].geometry.location);
        });
      });
    }
  }, [autocomplete, map, searchTextField, geocoder]);

  useEffect(() => {
    if(googleMap && mapContainerRef) {
      const map = new googleMap.maps.Map(
        mapContainerRef.current,
        initialConfig
      );

      if (!props.readonly) {
        map.addListener('click', (e) => {
          handleLocationChange(e.latLng);
        });
      }

      setMap(map);
      setGeocoder(new googleMap.maps.Geocoder);
      setMarker(new googleMap.maps.Marker({ map: map }));
    }
  }, [googleMap])

  useEffect(() => {
    if (map && locationsStore.selectedLocation) {
      const location = locationsStore.selectedLocation.coordinates;

      marker.setPosition(location);
      if (props.onCoordinatesChange) {
        props.onCoordinatesChange(locationsStore.selectedLocation);
      }
      map.panTo(location);

      geocoder.geocode({'location': {lat: location.lat, lng: location.lng}}, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            const address = results[0].formatted_address
            searchTextField.current.value = address;
          } else {
            console.log('No results found');
          }
        } else {
          console.log('Geocoder failed due to: ' + status);
        }
      });

    }
  }, [locationsStore.selectedLocations, map]);

  useEffect(() => {
    if (map && locationsStore.selectedLocation) {
      const location = locationsStore.selectedLocation.coordinates;

      marker.setPosition(location);
      if (props.onCoordinatesChange) {
        props.onCoordinatesChange(locationsStore.selectedLocation);
      }
      map.panTo(location);
      geocoder.geocode({'location': {lat: location.lat, lng: location.lng}}, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            const address = results[0].formatted_address
            searchTextField.current.value = address;
          } else {
            console.log('No results found');
          }
        } else {
          console.log('Geocoder failed due to: ' + status);
        }
      });
    }
  }, [locationsStore.selectedLocation]);

  const handleLocationChange = (location)=> {
    if (typeof location.lat === "function") {
      locationsStore.setSelectedLocation({...locationsStore.selectedLocation, coordinates: {lat: location.lat(), lng: location.lng()}});
    }
    else {
      locationsStore.setSelectedLocation({...locationsStore.selectedLocation, coordinates: location});
    }
    //marker.setVisible(true);
    //setIsMarkerShown(true);
  }

  return (
    <React.Fragment>
      <div
        style={{
          height: "50vh",
          width: "50%"
        }}
        ref={mapContainerRef}
      />
      <div
        style={{
          marginTtop: "20px"
        }}
      >
        <input
          type="text"
          id="searchTextField"
          ref={searchTextField}
          style={{
            height: 40,
            width: "60%"
          }}
          disabled={props.readonly}
        />
      </div>
    </React.Fragment>
  )
}

export default inject('rootStore')(observer(NewLocationMap));
