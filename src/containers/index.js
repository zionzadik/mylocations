import HomePage from "./HomePage";
import LocationsPage from "./LocationsPage";

export {
  HomePage,
  LocationsPage,
}
