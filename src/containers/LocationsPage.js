import React from "react";
import { inject, observer } from 'mobx-react';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

//import CategoriesList from '../components/categories/CategoriesList';
import LocationPanels from '../components/locations/LocationPanels';
import GroupedLocationsList from '../components/locations/GroupedLocationsList';
import Menu from '../components/Menu';
import SimpleLocationsList from '../components/locations/SimpleLocationsList';
import LocationsListPanel from '../components/locations/LocationsListPanel';

const LocationsPage = (props) => {
  const { rootStore: { locationsStore, UIStore }} = props;
  const [ selectedMenu, setSelectedMenu ] = React.useState(0);

  const handleMenuChange = (value) => {
    setSelectedMenu(value);
  }
  
  return (
    <Grid container spacing={2}>
      <Grid item xs={2}>
        <Typography variant="h5">
          Locations
        </Typography>
        <LocationsListPanel />
        { UIStore.viewType === "grouped" &&
          <GroupedLocationsList />
        }
        { UIStore.viewType === "list" &&
          <SimpleLocationsList />
        }
      </Grid>
      <Grid item xs={10}>
        <Menu contextType={"Location"} context={locationsStore.selectedLocation} onChange={handleMenuChange}/>
        <LocationPanels value={selectedMenu}/>
      </Grid>
    </Grid>
  )
}

export default inject('rootStore')(observer(LocationsPage));
