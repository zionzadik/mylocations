import React from "react";
import { inject, observer } from 'mobx-react';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import CategoriesList from '../components/categories/CategoriesList';
import CategoryPanels from '../components/categories/CategoryPanels';

import Menu from '../components/Menu';

const HomePage = (props) => {
  const { rootStore: { categoriesStore }} = props;
  const [ selectedMenu, setSelectedMenu ] = React.useState(0);

  const handleMenuChange = (value) => {
    setSelectedMenu(value);
  }

  React.useEffect(() => {
    setSelectedMenu(0);
  }, [categoriesStore.selectedCategory]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={2}>
        <Typography variant="h5">
          Categories
        </Typography>
        <CategoriesList />
      </Grid>
      <Grid item xs={10}>
        <Menu contextType={"Category"} context={categoriesStore.selectedCategory} onChange={handleMenuChange}/>
        <CategoryPanels value={selectedMenu}/>
      </Grid>
    </Grid>
  )
}

export default inject('rootStore')(observer(HomePage));
