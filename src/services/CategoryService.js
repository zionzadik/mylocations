const CATEGORIES = "myLocations.categories";
export default class CategoryService {
  storage = null;

  constructor(storage) {
    this.storage = storage;
  }

  load = () => {
    if (this.storage) {
      // read categories from local storage
      const categories = this.storage.get(CATEGORIES) ? this.storage.get(CATEGORIES) : [];
      return categories;
    }
  }

  add = (category) => {
    const categories = this.load();

    if (this.indexOf(categories, "name", category.name) > -1) {
      throw new Error("A category with this name already exists.");
    }

    categories.push(category);
    categories.sort((a, b) => a.name.localeCompare(b.name));

    this.storage.set(CATEGORIES, categories);

    return categories;
  }

  replace = (categoryId, name)=> {
    const categories = this.load();

    var foundIndex = this.indexOf(categories, "id", categoryId);
    categories[foundIndex].name = name;

    this.storage.set(CATEGORIES, categories);

    return categories;
  }

  delete = (category) => {
    const categories = this.load();
    const categoryId = category.id;

    const foundIndex = this.indexOf(categories, "name", category.name);


    categories.splice(foundIndex, 1);

    this.storage.set(CATEGORIES, categories);

    return categories;
  }

  indexOf = (categories, property, value) => {
    const foundIndex = categories.findIndex(item => item[property] === value);

    return foundIndex;
  }

  get = (id) => {
    const categories = this.load();
    const foundIndex = this.indexOf(categories, "id", id);

    return categories[foundIndex];
  }
}
