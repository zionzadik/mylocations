const LOCATIONS = "myLocations.locations";
export default class LocationService {
  storage = null;

  constructor(storage) {
    this.storage = storage;
  }

  sort = (locations, isAsc) => {
    if (isAsc) {
      return locations.slice().sort((a, b) => a.name.localeCompare(b.name));
    }
    else {
      return locations.slice().sort((a, b) => b.name.localeCompare(a.name));
    }
  }

  load = () => {
    if (this.storage) {
      // read categories from local storage
      const locations = this.storage.get(LOCATIONS) ? this.storage.get(LOCATIONS) : [];
      return locations;
    }
  }

  add = (location) => {
    const locations = this.load();

    //TODO: probably need to change how this check is performed
    if (this.indexOf(locations, location.id) > -1) {
      throw new Error("A location with this name already exists.");
    }

    locations.push(location);
    locations.sort((a, b) => a.name.localeCompare(b.name));

    this.storage.set(LOCATIONS, locations);

    return locations;
  }

  replace = (location, newLocation)=> {
    const locations = this.load();

    var foundIndex = this.indexOf(locations, location.id);
    locations[foundIndex] = newLocation;

    this.storage.set(LOCATIONS, locations);

    return locations;
  }

  delete = (location) => {
    const locations = this.load();

    const foundIndex = this.indexOf(locations, location.id);
    locations.splice(foundIndex, 1);

    this.storage.set(LOCATIONS, locations);

    return locations;
  }

  indexOf = (locations, id) => {
    const foundIndex = locations.findIndex(item => item.id === id);

    return foundIndex;
  }

  deleteByCategory = (list, category) => {
    let locations=[];

    list.forEach((item)=> {
      const categories = item.categories;
      if (categories.includes(category.id)) {
        const foundIndex = categories.indexOf(category.id);
        item.categories.splice(foundIndex, 1);
        locations = this.replace(item, item);
      }
    })

    return locations;
  }

  groupByCategory = (list, keyGetter, categoryNameGetter, sortAsc) => {
    const map = new Map();
    list.forEach((item) => {
      debugger;

      let categories = keyGetter(item);
      if (categories.length === 0) {
        categories = [{id: "0", name: "Orphans (No Category Set)"}];
      }


      categories.forEach((category) => {
        const fullCategory = categoryNameGetter(category);
        const categoryName = fullCategory ? fullCategory.name : category.name;
        const collection = map.get(categoryName);
        if (!collection) {
           map.set(categoryName, [item]);
        } else {
           collection.push(item);
           if (sortAsc)
             collection.sort((a, b) => a.name.localeCompare(b.name));
           else {
             collection.sort((a, b) => b.name.localeCompare(a.name));
           }
        }
      })
    });

    if (sortAsc)
      return new Map([...map.entries()].sort((a, b) => a[0].localeCompare(b[0])));
    else {
      return new Map([...map.entries()].sort((a, b) => b[0].localeCompare(a[0])));
    }
  }
}
