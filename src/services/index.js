import store from 'store';

import CategoryService from './CategoryService';
import LocationService from './LocationService';

import MenuService from './MenuService';

const services = {
  categoryService: new CategoryService(store),
  locationService:new LocationService(store),

  menuService: new MenuService(),
}

export default services;
