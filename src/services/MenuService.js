import React from 'react';

import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import PageviewIcon from '@material-ui/icons/Pageview';

const MenusArr = {
  "Category": [
    {
      label: "New Category",
      index: 0,
      icon: <AddIcon />,
      isActive: (context) => {
        return true;
      }
    },
    {
      label: "Edit Category",
      index: 1,
      icon: <EditIcon />,
      isActive: (context) => {
        return (typeof context !== "undefined");
      }
    },
    {
      label: "Delete Category",
      index: 2,
      icon: <DeleteForeverIcon />,
      isActive: (context) => {
        return (typeof context !== "undefined");
      }
    },
    {
      label: "View Details",
      index: 3,
      icon:<PageviewIcon />,
      isActive: (context) => {
        return (typeof context !== "undefined");
      }
    },
  ],
  "Location": [
    {
      label: "New Location",
      index: 0,
      icon: <AddIcon />,
      isActive: (context) => {
        return true;
      }
    },
    {
      label: "Edit Location",
      index: 1,
      icon: <EditIcon />,
      isActive: (context) => {
        return (typeof context !== "undefined");
      }
    },
    {
      label: "Delete Location",
      index: 2,
      icon: <DeleteForeverIcon />,
      isActive: (context) => {
        return (typeof context !== "undefined");
      }
    },
    {
      label: "View Details",
      index: 3,
      icon:<PageviewIcon />,
      isActive: (context) => {
        return (typeof context !== "undefined");
      }
    }
  ],
}

export default class MenuService {
  load = (contextType) => {
    return MenusArr[contextType];
  }
}
