import React from 'react';
import { inject } from 'mobx-react';
import { RouterView } from 'mobx-state-router';

import Notification from './components/Notification';

// import DevTools from 'mobx-react-devtools';
import {
  HomePage,
  LocationsPage,
} from './containers';

const viewMap = {
  'home': <HomePage />,
  'locations': <LocationsPage/>
};

const ShellBase = (props) => {
  const { rootStore: { routerStore }} = props;

  return (
    <div>
      <Notification />
      <RouterView routerStore={routerStore} viewMap={viewMap} />
    </div>
  );
}
export const Shell = inject('rootStore')(ShellBase);
